Library 
=====

An OTP application

Build and start
-----

    $ rebar3 shell
    $ application:start(library). 
    $ db:init(). //jeśli baza nie została zainicjalizowana
    $ application:start(mnesia).
    
  Opis i użycie 
 -----
    Aplikacja symuluje działanie bibloteki, możemy dodawać, wypozyczać, 
    zwracać ksiązki itp. oraz rejestrować użytkowników.Serwer zapamiętuje użytkownika i w trakcie jego 
    działania nie trzeba podawać swojej nazwy. Zarejestrowanie się jako klient jest jednak konieczne przy
    starcie serwera.
   
    
`new_client(Name)` - rejestruje pid i nazwe klienta  
`new_account(Name)`- rejestruje nowe konton w bazie oraz automatycznie pid klienta  
`queue_seq([{Name,[fun/1],[arg]}..])` / `queue_con([{Name,[fun/1],[arg]}..])` -symuluje sekwencyjną lub 
współbieżną kolejke obsługiwanych klientów, przyjmuje liste
krotek składającą się z nazwy klienta, listy funkcji, listy argumentów.

Z reszty funkcji możemu korzystać po zarejestrowaniu pida.  

`book_to_return()`  
`lend(Title)`  
`is_book(Title)`  
`return(Title)`  
`new_book(Id,Title,Author)`  


