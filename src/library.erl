%%%-------------------------------------------------------------------
%%% @author Linus
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 19. sty 2018 22:56
%%%-------------------------------------------------------------------
-module(library).
-author("Patryk Zygmunt").

-behaviour(gen_server).

%% API
-export([start_link/0, queue_seq/1,client_seq/3,lend/1,return/1,new_account/1,new_book/3,
  test/0,test2/0,book_to_return/0,is_book/1,new_client/1,queue_con/1,client_con/3]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(state,{name,pid}).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @end
%%--------------------------------------------------------------------
-spec(start_link() ->
  {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link() ->
  gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
-spec(init(Args :: term()) ->
  {ok, State :: #state{}} | {ok, State :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term()} | ignore).
init([]) ->
  {ok, #{12=>#state{pid=12,name = "ja"}}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
    State :: #state{}) ->
  {reply, Reply :: term(), NewState :: #state{}} |
  {reply, Reply :: term(), NewState :: #state{}, timeout() | hibernate} |
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), Reply :: term(), NewState :: #state{}} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_call({lend,{Title}}, _From, State) ->
  {Pid,_}=_From,
  User = maps:get(Pid,State),
  {reply, service:lend(User#state.name,Title), State};

handle_call({return,{Title}}, _From, State) ->
  {Pid,_}=_From,
  User = maps:get(Pid,State),
  {reply, service:return(User#state.name,Title), State};
handle_call({book_to_return,_}, _From, State) ->
  {Pid,_}=_From,
  User = maps:get(Pid,State),
  {reply, service:book_to_return(User#state.name), State};

handle_call({new_account,{Name}}, _From, State) ->
  {reply, service:newAccount(Name), State};

handle_call({is_book,{Title}}, _From, State) ->
  {reply, service:is_book(Title), State};

handle_call({client,{Name}},_From, State) ->
  {Pid,_}=_From,
  {reply, ok,maps:put(Pid,#state{name = Name,pid =Pid},State)}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).

handle_cast({new_book,{Id,Title,Author}}, State) ->
  service:add_book(Id,Title,Author),
  {noreply,State}.


%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_info(_Info, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
    State :: #state{}) -> term()).
terminate(_Reason, _State) ->
  ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
-spec(code_change(OldVsn :: term() | {down, term()}, State :: #state{},
    Extra :: term()) ->
  {ok, NewState :: #state{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

response_clients([],R)->R;
response_clients([_|Cl],Res)->
  receive R->response_clients(Cl,Res ++ [R])
  end.


%%%===================================================================
%%% Internal functions
%%%===================================================================
client_seq(Name,Fl,Arg)->
  new_client(Name),
  {Name ,lists:zipwith(fun(F,A)-> F(A) end,Fl,Arg)}.

new_client(Name)->gen_server:call(?MODULE,{client,{Name}}).

client_con(Name,Fl,Arg)->
  new_client(Name),
  queue ! {Name , lists:zipwith(fun(F,A)-> F(A) end,Fl,Arg)}.

new_account(Name)->gen_server:call(?MODULE,{new_account,{Name}}),new_client(Name).
book_to_return()->gen_server:call(?MODULE,{book_to_return,{ok}}).
lend(Title)->gen_server:call(?MODULE,{lend,{Title}}).
is_book(Title)->gen_server:call(?MODULE,{is_book,{Title}}).
return(Title)->gen_server:call(?MODULE,{return,{Title}}).
new_book(Id,Title,Author)->gen_server:cast(?MODULE,{new_book,{Id,Title,Author}}).

queue_seq(Cl)->lists:map(fun(C)->{Name,Fl,Arg} = C,client_seq(Name,Fl,Arg) end ,Cl).
queue_con(Cl)->register(queue,self()),
  lists:map(fun(C)->{Name,Fl,Arg} = C,spawn(library,client_con,[Name,Fl,Arg]) end ,Cl),
  response_clients(Cl,[]).


test()->{"name",[fun library:return/1,fun library:return/1],["title","title"]}.
test2()->{"name",[fun library:is_book/1,fun library:return/1],["title","title"]}.