
-module(db).
-compile([export_all]).

-record(book,{id,title,author,lend, lendBy,date}).
-record(user,{login,date}).

init()->
  mnesia:stop(),
    mnesia:create_schema([node()]),
  mnesia:delete_table(book),
 mnesia:delete_table(user),
  application:start(mnesia),
  mnesia:create_table(book,[{ram_copies,[node()]},{attributes, record_info(fields, book)},{type, set}]),
  mnesia:create_table(user,[{ram_copies,[node()]},{attributes, record_info(fields, user)},{type, set}]).

start()->
  application:start(mnesia),timer:sleep(300).

addNewBook(Id,Title,Author)->
    Book = #book{id =Id,title=Title,author = Author,lend = false, lendBy = "", date = date()} ,
    mnesia:activity(transaction,fun() -> mnesia:write(Book) end).

getBooksLendBy(User)->
                   Book = #book{_ = '_',lend = true,lendBy = User,_ = '_'},
                   mnesia:activity(transaction,fun() -> mnesia:match_object(Book) end).
getBookLendByWithTitle(User,Title)->
                   Book = #book{_ = '_',title = Title,_ = '_',lendBy = User,_ = '_'},
                   mnesia:activity(transaction,fun() -> mnesia:match_object(Book) end).
getBooksByTitle(Title)->
                   Book = #book{title = Title,_ = '_',lend = false,_ = '_'},
                   mnesia:activity(transaction,fun() -> mnesia:match_object(Book) end).
getBooksNotLend(Title)->
                   Book = #book{title = Title,_ = '_'},
                   mnesia:activity(transaction,fun() -> mnesia:match_object(Book) end).

lendBook(User,Title)->
  [Book|_] = getBooksNotLend(Title),
  LendBook = #book{id = Book#book.id,
                   title = Title,
                   author = Book#book.author,
                   lend = true,
                   lendBy = User,
                   date = date()},
  addBook(LendBook).


returnBook(User,Title)-> 
          [Book|_] = getBookLendByWithTitle(User,Title),
          LendBook = #book{id = Book#book.id,
                   title = Title,
                   author = Book#book.author,
                   lend = false,
                   lendBy = "",
                   date = date()},
          addBook(LendBook).

addBook(Book)-> mnesia:activity(transaction,fun() -> mnesia:write(Book) end).

deleteBook(Title)->
  mnesia:activity(transaction,fun() -> mnesia:delete(book,Title) end).

getBook(Id)->
 mnesia:activity(transaction,fun() -> mnesia:read(book,Id) end).

newUser(Name)->
  User = #user{login=Name,date=date()},
  mnesia:activity(transaction,fun() -> mnesia:write(User) end).

getUser(Name)->
  mnesia:activity(transaction,fun() -> mnesia:read(user,Name) end).

