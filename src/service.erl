-module(service).
-compile([export_all]).



start()->
      db:dbStart().
init()->
      db:init().

lend(User, Title) ->
  U = db:getUser(User),
  B = db:getBooksByTitle(Title),
  UB = db:getBooksLendBy(User),
  BL = db:getBooksNotLend(Title),
 
  if U == [] -> "You don't have account";
     B == [] -> "Sorry we dont have this book";
     BL == [] -> "Sorry someone lend this book";
     UB == [] -> db:lendBook(User,Title);
    true -> "You shoul return book"
  end.     

return(User, Title) ->
  BL = db:getBookLendByWithTitle(User,Title),
    %calendar:time_difference(DT1, DT2)
  if 
     BL == [] -> "You dont lend this book";
     true -> 
            db:returnBook(User,Title),
            "OK, books to return " ++  integer_to_list(length(db:getBooksLendBy(User)))             
  end. 

newAccount(Name)->
        U = db:getUser(Name),
      if U == [] -> db:newUser(Name);
         true -> "Sorry login in used"
      end.

add_book(Id,Title,Author)->db:addNewBook(Id,Title,Author).

book_to_return(Name)->
  Books=db:getBooksLendBy(Name),
  "books to return " ++  integer_to_list(length(Books))++Books.


is_book(Title)->
  B = db:getBooksByTitle(Title),
  BL = db:getBooksNotLend(Title),
  if B == [] -> "Sorry we dont have this book";
    BL == [] -> "Sorry someone lend this book";
    true-> exist
  end.


getUser(Name)->
  U = db:getUser(Name),
  if U == [] -> "You dont have account";
    true -> U
  end.